import React from 'react'

function Sidebar({ className, scrollToSection, activeSection }) {
    const sections = [
        { id: 'services', title: 'Services' },
        { id: 'about', title: 'About Me' },
        { id: 'skills', title: 'Skills' },
        { id: 'experience', title: 'Experience' },
        { id: 'contact', title: 'Contact' },
    ]

    return (
        <aside className={`bg-white dark:bg-gray-950 p-4 overflow-y-auto ${className}`}>
            <nav className='w-64 flex-shrink-0 p-4'>
                <ul>
                    {sections.map((section) => (
                        <li key={section.id} className='mb-2'>
                            <button
                                onClick={() => scrollToSection(section.id)}
                                className={`text-gray-700 dark:text-gray-300 hover:underline focus:outline-none ${
                                    activeSection === section.id ? 'font-bold text-blue-600 dark:text-blue-400' : ''
                                }`}
                            >
                                {section.title}
                            </button>
                        </li>
                    ))}
                </ul>
            </nav>
        </aside>
    )
}

export default Sidebar
