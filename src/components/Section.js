import React from 'react'

function Section({ section }) {
    return (
        <section id={section.id} className='mb-12'>
            <h2 className='text-2xl font-semibold mb-4'>{section.title}</h2>
            <ul className='list-disc ml-5 space-y-2'>
                {section.items.map((item, index) => (
                    <li key={index}>{item}</li>
                ))}
            </ul>
        </section>
    )
}

export default Section
