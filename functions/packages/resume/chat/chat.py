import json, os

# from openai import OpenAI
# from anthropic import Anthropic


def main(event, context):
    # openai_client = OpenAI(
    #     api_key=os.environ.get("OPENAI_API_KEY"),
    # )

    print("Received event")
    # print(json.dumps(event, indent=4))

    out_of_credits_msg = """Looks like Jeremy is very popular this week, I don't have any more LLM credits to answer your question. Please scroll down and take a look at his resume."""

    return {"body": {"response": out_of_credits_msg}}

    # return {
    #     "body": {
    #         "event": event,
    #         "context": {
    #             "activationId": context.activation_id,
    #             "apiHost": context.api_host,
    #             "apiKey": context.api_key,
    #             "deadline": context.deadline,
    #             "functionName": context.function_name,
    #             "functionVersion": context.function_version,
    #             "namespace": context.namespace,
    #             "requestId": context.request_id,
    #         },
    #     },
    # }
